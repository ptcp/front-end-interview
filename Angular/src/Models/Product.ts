
export interface Inventory {
    productId: number;
    quantity: number;
}


export interface Product {
    id: number;
    name: string;
    description: string;
    unitCost: number;
}

export class ProductInventory {
    constructor(
        public product: Product,
        public inventory: Inventory) { }
}
