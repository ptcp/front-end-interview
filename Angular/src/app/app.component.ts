import { Component, OnInit } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable, of, forkJoin } from 'rxjs';
import { ProductInventory, Product, Inventory } from 'src/Models/Product';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent implements OnInit {
  title = 'front-end-interview';
  now = new Date();

  productInventories: Observable<ProductInventory[]>;

  constructor(private httpClient: HttpClient) { }

  ngOnInit() {
    // service calls
    this.httpClient.get<Array<Product>>('assets/products.json');
    this.httpClient.get<Array<Inventory>>('assets/inventories.json');
  }
}
