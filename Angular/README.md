# FrontEndInterview

Thank you for you interest in Performance Trust!  Below are a few tasks we would like to see how you solve and work through.  Feel free to use any resources at your disposal, e.g. google, stackoverflow, etc.

## Tasks
- Show a list of Products and their related Inventories on the screen.  The html has been stubbed out for you as well as the ProductInventory class.
- The Quantity isn't being shown in the table.  Fix the binding to show the available quantity of each product.
- Filter out Products from the table that cost over $450.
- Place the footer element into its own component and pass the Date into the new footer component from the App component.  
- Format the date in the footer in the following pattern: June 15, 2015