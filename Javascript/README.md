# FrontEndInterview_Javascript

Thank you for you interest in Performance Trust! The following are a series of Javascript tests you need to modify and make pass. The tests have been stubbed out with some basic first steps. Any further clarification that would've made the "it should..." too long are added as comments. Please ask questions if anything is confusing.

## Tasks
- Open test.html in your browser of choice and then the dev console to verify you have a slate of failing tests
- Start working at making the tests pass in your editor of choice. Feel free to use any resources at your disposal, e.g. google, stackoverflow, etc.
- At any point in your work you can save the test.js file and refresh the html to see how your tests are doing
- When you're done or time is exhausted (~30-45min), commit your work and push the feature to origin