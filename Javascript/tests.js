it('should pass the correct parameters to the "areEqual" fuction so it returns true', () => {
    const areEqual = (o1, o2) => {
        return o1 === o2;
    };
    assert.isTrue(areEqual(1, '1'));
});

it('should allow the variable "value" to be reassigned', () => {
    const value = 1;
    value = 2;
    assert.isTrue(value === 2);
});

it('should allow variable "context" to be accessible in both the setter() function and assert', () => {
    // change how the variable is declared
    const setContextToTwo = function () {
        var context = 2;
    };
    setContextToTwo();
    assert.isTrue(context === 2);
});

it('should return the sum of "numArray" from "sumFor" using a for loop', () => {
    const sumFor = (arr) => {
        return -1;
    };

    const numArray = [1, 2, 3, 4];
    assert.isTrue(sumFor(numArray) === 10);
});

it('should return the sum of "numArray" array from "sumForEach" using Array.forEach()', () => {
    const sumForEach = (arr) => {
        return -1;
    };

    const numArray = [1, 2, 3, 4];
    assert.isTrue(sumForEach(numArray) === 10);
});

it('should return the sum of "numArray" array using Array.reduce()', () => {
    const sumReduce = (arr) => {
        return arr.reduce(/*...*/);
    };

    const numArray = [1, 2, 3, 4];
    assert.isTrue(sumReduce(numArray) === 10);
});

it('should return true if numArray contains odd values', () => {
    const containsOdds = (arr) => {
        return false;
    };

    const arrayWithOdd = [1, 2];
    const arrayWithoutOdd = [2];

    assert.isTrue(containsOdds(arrayWithOdd));
    assert.isFalse(containsOdds(arrayWithoutOdd));
});

it('should return a new array of numbers that are triple the values in numArray', () => {
    const tripleNumbers = (array) => {
        return array;
    };

    const testArray = [1, 2, 3, 4];
    const resultArr = tripleNumbers(testArray);

    const expected = [3, 6, 9, 12];
    assert.isTrue(expected[0] === resultArr[0]);
    assert.isTrue(expected[1] === resultArr[1]);
    assert.isTrue(expected[2] === resultArr[2]);
    assert.isTrue(expected[3] === resultArr[3]);
});

it('should copy one object to the other', () => {
    const source = { a: 4, b: 3, c: 2, d: 1 };
    const destination = {};

    const destKeys = Object.keys(destination);
    const srcKeys = Object.keys(source);
    assert.isTrue(srcKeys.every(key => destKeys.includes(key)));
    assert.isTrue(srcKeys.every(key => destination[key] === source[key]));
});